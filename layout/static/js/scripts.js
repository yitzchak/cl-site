$(document).ready(function () {

  $('span.menu-link').click(function (event) {
    var nextMenu = $(this).next('.menu');
    console.log(nextMenu.siblings('.menu').slideUp());
    nextMenu.siblings('.menu').slideUp();
    nextMenu.slideToggle();
  });

  /* Random Lisp quote in footer */

  var $quotes = $('#quotes > div');
  var $quote = $($quotes[Math.floor(Math.random() * $quotes.length)]);

  $('#lisp-quote').html($quote.html());

  /* Announcement box */

  let announcementUrl = "https://common-lisp.net/broadcast.html";

  let closeMessageBoxBtn = $('#message-box-close');
  let openMessasgeBoxBtn = $('#message-hidden-btn');
  let messageBox = $('#message-box');
  let messageText = $('#message-box-text');
  let initialBodyPadding = $('body').outerHeight() - $('body').height();

  function calculateNewPadding(isMessageShowing) {
    let messageHeight = $('#message-box').outerHeight();
    return isMessageShowing ? initialBodyPadding + messageHeight : initialBodyPadding;
  }

  function closeMessageBox() {
    messageBox.css('display', 'none');
    openMessasgeBoxBtn.css('display', 'block');
    localStorage.setItem('was_message_closed', true);
    $('body').css('padding-top', calculateNewPadding(false));
  }

  function openMessageBox() {
    messageBox.css('display', 'flex');
    openMessasgeBoxBtn.css('display', 'none');
    localStorage.removeItem('was_message_closed');
    $('body').css('padding-top', calculateNewPadding(true));
  }

  closeMessageBoxBtn.click(function (event) {
    event.preventDefault();
    closeMessageBox();
  });

  openMessasgeBoxBtn.click(function (event) {
    event.preventDefault();
    openMessageBox();
  });

  $.ajax({
    url: announcementUrl,
    success: function (resp) {
      if (resp == null || resp.length < 1) { return; }
      messageText.html(resp);
      let previousMessage = localStorage.getItem('previous_message');
      if (previousMessage == null || previousMessage !== resp) {
        localStorage.setItem('previous_message', resp);
        openMessageBox();
        return;
      }

      let wasMessageClosed = localStorage.getItem('was_message_closed');
      if (wasMessageClosed) {
        closeMessageBox();
      } else {
        openMessageBox();
      }
    }
  })
});
